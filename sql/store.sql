-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le : ven. 04 déc. 2020 à 12:45
-- Version du serveur :  5.7.30
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données : `store`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id_categorie` int(11) NOT NULL,
  `nom_categorie` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `nom_categorie`) VALUES
(1, 'telephonie'),
(2, 'autres');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `id_commande` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `date_commande` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `prix_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id_commande`, `id_user`, `date_commande`, `prix_total`) VALUES
(5, 8, '2020-12-03 11:20:32', 808.98),
(6, 8, '2020-12-03 11:53:52', 796.86);

-- --------------------------------------------------------

--
-- Structure de la table `commande_produit`
--

CREATE TABLE `commande_produit` (
  `id_commande_produit` int(11) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `id_commande` int(11) NOT NULL,
  `prix_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commande_produit`
--

INSERT INTO `commande_produit` (`id_commande_produit`, `id_produit`, `id_commande`, `prix_total`) VALUES
(3, 9, 5, 789),
(4, 10, 5, 12),
(5, 9, 6, 789);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE `produit` (
  `id_produit` int(11) NOT NULL,
  `id_categorie` int(11) DEFAULT NULL,
  `nom_produit` varchar(255) NOT NULL,
  `description_produit` varchar(400) NOT NULL,
  `desc_courte_produit` varchar(255) NOT NULL,
  `prix` float NOT NULL,
  `qte` int(11) NOT NULL,
  `images` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `produit`
--

INSERT INTO `produit` (`id_produit`, `id_categorie`, `nom_produit`, `description_produit`, `desc_courte_produit`, `prix`, `qte`, `images`, `id_user`) VALUES
(2, 1, 'iphoneX', 'vend iphoneX pas cher super état du tout', 'Super etat rien a dire du tout du tout', 529, 1, 'back/img/iphone-5.png', 8),
(5, 1, 'iphoneX', 'vend iphoneX ', 'Super etat rien a dire du tout', 529, 1, 'back/img/iphone-5.png', 8),
(9, 1, 'iphone 10 parfait', 'je vends un iphone de qualité pour un prix de qualité', 'super état tout ça tout ça !! ', 789, 1, 'back/img/iphone-6.png', 8),
(10, 1, 'anthony', 'Demo', 'GFSQGFQGQFDGQF', 12, 1, '', 8);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id_user` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `email` varchar(50) NOT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `complement_adresse` varchar(255) DEFAULT NULL,
  `code_postal` int(11) DEFAULT NULL,
  `commune` varchar(255) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT '0',
  `isBanned` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id_user`, `username`, `password`, `nom`, `prenom`, `email`, `adresse`, `complement_adresse`, `code_postal`, `commune`, `isAdmin`, `isBanned`) VALUES
(7, 'anthony', '$2y$10$dzcgPO1CuwMu1Nqru.930OYcO7P0QidlyMB9iUXHu.EkPcz1pygJ.', 'testtest', 'testtest', 'anthony@exemple.com', NULL, NULL, NULL, NULL, 0, 0),
(8, 'Nicolas', '$2y$10$LZ.kFUYMEWPK7mU8ALo7p.xXXzyj0hMo8KY7Gd9se23EcrVciWbfa', 'gelin', 'nicolas', 'nicolas@example.com', '4 rue du test', 'appt 001', 22340, 'bourbriac', 0, 0),
(9, 'testtest', '$2y$10$huYYbSKEDFXJLE1AjpDM.uNdysos2oBl2xDKP7PmzDrn83YMlQgFO', 'test', 'test', 'test@exemple.com', NULL, NULL, NULL, NULL, 0, 0),
(10, 'admin', 'admin', 'admin', 'admin', 'admin@exemple.com', NULL, NULL, NULL, NULL, 1, 0),
(11, 'demo11', '$2y$10$qXMsPDjDa.JH2HZusJbul.6BRfM4CZNOWLcoSg/WIuEtSFgj9Tu9a', 'demo', 'demo', 'demo@exemple.com', NULL, NULL, NULL, NULL, 0, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`id_commande`),
  ADD KEY `fk_id_user` (`id_user`);

--
-- Index pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD PRIMARY KEY (`id_commande_produit`),
  ADD KEY `fk_id_commande_produit` (`id_commande`),
  ADD KEY `id_produit` (`id_produit`);

--
-- Index pour la table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`id_produit`),
  ADD KEY `fk_id_categorie` (`id_categorie`),
  ADD KEY `id_user` (`id_user`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `id_commande` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  MODIFY `id_commande_produit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `produit`
--
ALTER TABLE `produit`
  MODIFY `id_produit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `commande_produit`
--
ALTER TABLE `commande_produit`
  ADD CONSTRAINT `commande_produit_ibfk_1` FOREIGN KEY (`id_produit`) REFERENCES `produit` (`id_produit`),
  ADD CONSTRAINT `commande_produit_ibfk_2` FOREIGN KEY (`id_commande`) REFERENCES `commande` (`id_commande`);

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `fk_id_categorie` FOREIGN KEY (`id_categorie`) REFERENCES `categorie` (`id_categorie`),
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `utilisateur` (`id_user`);
