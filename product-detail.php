<?php
require_once 'back/lib/inc.php';
require 'back/lib/functions.php';
session_start();
if (isset($_GET['id_produit'])) {
    $id_produit = assainirEntier($_GET['id_produit']);
    $informations = Product::construit_product($id_produit);
}

if (!empty($_POST)) {
    $commerceCard = new Commerce();
    $produit_id = assainirEntier($_POST['id_produit']);
    $commerceCard->addCart($produit_id);
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="eCommerce HTML Template Free Download" name="keywords">
    <meta content="eCommerce HTML Template Free Download" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php'?>

    <!-- Product Detail Start -->
    <div class="product-detail">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="product-detail-top">
                        <div class="row align-items-center">
                            <div class="col-md-5">
                                <div class="product-slider-single">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                    <img src="<?=$informations->images?>" alt="Product Image">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="product-content">
                                    <div class="title">
                                        <h2><?=$informations->nom_produit?></h2>
                                    </div>
                                    <div class="price">
                                        <h4>Prix:</h4>
                                        <p>€<?=$informations->prix?></p>
                                    </div>
                                    <div class="action">
                                        <form action="" method="post">
                                            <input type="hidden" name="id_produit"
                                                value="<?=$informations->id_produit?>">
                                            <button class="btn" type="submit"><i class="fa fa-shopping-cart"></i>Ajouter
                                                au panier</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row product-detail-bottom">
                        <div class="col-lg-12">
                            <ul class="nav nav-pills nav-justified">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="pill" href="#description">Description</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div id="description" class="container tab-pane active">
                                    <p>
                                        <?=$informations->description_produit?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Product Detail End -->

    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>