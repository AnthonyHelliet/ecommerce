<?php
function assainirString($key)
{
    if (isset($key)) {
        return strip_tags(htmlspecialchars($key));
    }
}

function assainirEntier($key)
{
    if (isset($key)) {
        return strip_tags(htmlspecialchars(intval($key)));
    }
}
function assainirFloat($key)
{
    if (isset($key)) {
        return strip_tags(htmlspecialchars(floatval($key)));
    }
}

function upload($file_to_upload)
{
    if (($file_to_upload != "")) {
        $counter = 0;
        $target_dir = "img/";
        if (!is_dir($target_dir)) {
            mkdir($target_dir);
        }
        $file = $file_to_upload;
        $path = pathinfo($file);
        $filename = $path['filename'];
        $ext = $path['extension'];
        $temp_name = $file_to_upload;
        $path_filename_ext = $target_dir . $filename . "." . $ext;
        if ($ext == "png" || $ext == "jpg") {
            if (file_exists($path_filename_ext)) {
                $i = 1;
                $new_path = $path_filename_ext;

                while (file_exists($new_path)) {
                    $new_filename = $filename . '-' . $i . '.' . $ext;
                    $new_path = $target_dir . $new_filename;
                    $new_path;
                    $i++;
                }
                move_uploaded_file($temp_name, $new_path);
                echo "fichier deja existant, copie sauvegarde avec succès !";
                echo $new_path;
                return $new_path;
            } else {
                move_uploaded_file($temp_name, $path_filename_ext);
                echo "fichier sauvegarde avec succès !";
                echo $path_filename_ext;
                return $path_filename_ext;
            }
        } else {
            echo 'votre fichier doit être une image ! ';
        }
    } else {
        echo "verifiez le contenu de votre fichier, nom vide ?";
    }
    // Check if file already exists
}