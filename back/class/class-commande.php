<?php
class Commande
{
    public $id_commande;
    public $id_user;
    public $date_commande;
    public $prix_total;

    public function __construct()
    {
        $this->id_commande = 0;
        $this->id_user = 0;
        $this->date_commande = new DateTime($this->date_commande);
        $this->prix_total = 0.0;
    }

    public static function construit_commande($id)
    {
        $commande = new Commande();
        $commande->load($id);
        return $commande;
    }
    public function load($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT * FROM commande_produit
        JOIN commande ON commande_produit.id_commande = commande.id_commande
        WHERE commande_produit.id_commande = :id_commande
        ");
        $stmt->execute(array(
            ":id_commande" => $id,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }

    public function save()
    {
        try {
            $db_connect = db_connect::getInstance();
            $stmt = $db_connect->prepare("INSERT INTO commande (id_user,prix_total)
                VALUES(:id_user,:prix_total)");
            $stmt->execute(array(
                ":id_user" => $this->id_user,
                ":prix_total" => $this->prix_total,
            ));
            $this->id_commande = $db_connect->lastInsertId();
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

}