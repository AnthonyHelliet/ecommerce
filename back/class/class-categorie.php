<?php
class Categorie
{
    public $id_categorie;
    public $nom_categorie;

    public function __construct()
    {
        $this->id_categorie = 0;
        $this->nom_categorie = "";
    }

    public static function construit_categorie($id)
    {
        $categorie = new Categorie();
        $categorie->load($id);
        return $categorie;
    }
    public function load($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT * FROM categorie WHERE id_categorie=:id_categorie");
        $stmt->execute(array(
            "id_categorie" => $id,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }
}