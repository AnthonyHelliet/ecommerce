<?php
require_once "back/class/class-singleton.php";

class User
{
    public $id_user;
    public $username;
    public $password;
    public $nom;
    public $prenom;
    public $email;
    public $adresse;
    public $complement_adresse;
    public $code_postal;
    public $commune;
    public $isBanned;

    public function __construct()
    {
        $this->id_user = 0;
        $this->username = "";
        $this->password = "";
        $this->nom = "";
        $this->prenom = "";
        $this->email = "";
        $this->adresse = "";
        $this->complement_adresse = "";
        $this->code_postal = 0;
        $this->commune = "";
        $this->isBanned = 0;
    }

    public static function construit_user ($id) {
        $user = new User();
        $user->load($id);
        return $user;
    }

    public function save()
    {
        $db_connect = db_connect::getInstance();

        if ($this->id_user) {
            //modification
            $stmt = $db_connect->prepare("UPDATE utilisateur SET username=:username, nom = :nom, prenom=:prenom, email=:email,
            adresse=:adresse, complement_adresse=:complement_adresse, code_postal=:code_postal, commune=:commune, isBanned=:isBanned WHERE id_user=:id_user");
            $stmt->execute(array(
                ":username" => $this->username,
                ":nom" => $this->nom,
                ":prenom" => $this->prenom,
                ":email" => $this->email,
                ":adresse" => $this->adresse,
                ":complement_adresse" => $this->complement_adresse,
                ":code_postal" => $this->code_postal,
                ":commune" => $this->commune,
                ":id_user" => $this->id_user,

            ));
            $_SESSION['user'] = $this;
        } else {
            //creation
            echo $this->email;
            $requete = "INSERT INTO utilisateur (username, password, nom, prenom, email) VALUES (:username, :password, :nom, :prenom, :email)";
            $stmt = $db_connect->prepare($requete);
            echo $stmt->execute(
                array(
                    ":username" => $this->username,
                    ":password" => password_hash($this->password, PASSWORD_DEFAULT),
                    ":nom" => $this->nom,
                    ":prenom" => $this->prenom,
                    ":email" => $this->email,
                ));
        }
    }

    public function update_password($password, $new_password)
    {
        $db_connect = db_connect::getInstance();
        if ($this->id_user) {
            $stmt = $db_connect->prepare("SELECT password FROM utilisateur WHERE id_user=:id_user");
            $stmt->execute(array(
                ":id_user" => $this->id_user,
            ));
            $stmt->bindColumn('password', $pass);
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            $pass = $row['password'];
            if (password_verify($password, $pass)) {
                $hash = password_hash($new_password, PASSWORD_DEFAULT);
                $stmt = $db_connect->prepare("UPDATE utilisateur SET password=:password WHERE id_user=:id_user");
                $stmt->execute(array(
                    ":password" => $hash,
                    ":id_user" => $this->id_user,
                ));
            }
        }
    }

    public function connect($password)
    {
        $db_connect = db_connect::getInstance();
        $requete = "SELECT id_user, username, password, nom, prenom, email, adresse, complement_adresse, code_postal, commune, isBanned  FROM utilisateur WHERE email=:email";
        $stmt = $db_connect->prepare($requete);
        $stmt->execute(array(
            ":email" => $this->email,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
        if (password_verify($password, $this->password)) {
            echo "ok";
            //Je sauve l'objet dans la session
            $_SESSION['user'] = $this;

            header('Location: index.php');
        } else {
            //Le mot de passe est différent
            return false;
        }
    }

    public function load($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT * FROM utilisateur WHERE id_user = :id_user ");
        $stmt->execute(array(
            ":id_user" => $id,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }

    public function isBanned($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("UPDATE utilisateur SET isBanned = !isBanned WHERE id_user = :id_user");
        $stmt->execute(array(
            ":id_user" => $id,
        ));

    }
}