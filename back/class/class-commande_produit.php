<?php
class Commande_produit
{
    public $id_commande_produit;
    public $id_produit;
    public $id_commande;
    public $prix_total;

    public function __construct()
    {
        $this->id_commande_produit = 0;
        $this->id_produit = 0;
        $this->id_commande = 0;
        $this->prix_total = 0.0;
    }

    public function save()
    {
        try {
            $db_connect = db_connect::getInstance();
            $stmt = $db_connect->prepare("INSERT INTO commande_produit (id_produit,id_commande,prix_total)
                VALUES(:id_produit,:id_commande,:prix_total)");
            $stmt->execute(array(
                ":id_produit" => $this->id_produit,
                ":id_commande" => $this->id_commande,
                ":prix_total" => $this->prix_total,
            ));
        } catch (Exception $e) {
            print $e->getMessage();
        }
    }

}