<?php
require_once 'back/lib/inc.php';
require_once 'back/lib/functions.php';
session_start();

// Si une session existe alors on redirige l'utilisateur vers l'index
if (isset($_SESSION['user'])) {
        header('Location: index.php');
        exit();
}

//Traitement du POST
if (!empty($_POST)) {

    if (isset($_POST['connect'])) {
        //connexion
        $user = new User();
        $user->email = assainirString($_POST['login']);
        $user->password = assainirString($_POST['password']);
        $user->connect($user->password);
    }

    if (isset($_POST['subscribe'])) {
        //inscription
        $user = new User();
        $user->username = assainirString($_POST['username']);
        $user->email = assainirString($_POST['mail']);
        $user->nom = assainirString($_POST['nom']);
        $user->prenom = assainirString($_POST['prenom']);
        $user->password = assainirString($_POST['password']);
        $user->mail = assainirString($_POST['mail']);
        $user->save();
    }
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php'?>

    <!-- Login Start -->
    <div class="login">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <form class="register-form" id="subscribe" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Nom d'utilisateur</label>
                                <input class="form-control" type="text" placeholder="Nom d'utilisateur" name="username"
                                    required minlength="6">
                            </div>
                            <div class="col-md-4">
                                <label>Nom</label>
                                <input class="form-control" type="text" placeholder="Nom" name="nom" required>
                            </div>
                            <div class="col-md-4">
                                <label>Prénom</label>
                                <input class="form-control" type="text" placeholder="Prénom" name="prenom" required>
                            </div>
                            <div class="col-md-12">
                                <label>Email</label>
                                <input class="form-control" type="email" placeholder="Email" name="mail" required
                                    pattern=".+@globex.com">
                            </div>
                            <div class="col-md-6">
                                <label>Mot de passe</label>
                                <input class="form-control" type="password" placeholder="Votre mot de passe"
                                    name="password" id="password" required>
                            </div>
                            <div class="col-md-6">
                                <label>Verification du mot de passe</label>
                                <input class="form-control" type="password" placeholder="Confirmez votre mot de passe"
                                    name="password_repeat" id="password_repeat" equalTo='#password' required>
                            </div>
                            <div class="col-md-12">
                                <button class="btn" type="submit" form="subscribe" name="subscribe">S'inscrire</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-6">
                    <form class="login-form" id="connect" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Votre email</label>
                                <input class="form-control" type="email" placeholder="Email" name="login"
                                    id="user_email">
                            </div>
                            <div class="col-md-6">
                                <label>Mot de passe</label>
                                <input class="form-control" type="password" placeholder="Mot de passe" name="password"
                                    id="user_password">
                            </div>
                            <div class="col-md-12">
                                <button class="btn" type="submit" form="connect" name="connect">Se connecter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Login End -->

    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>
    <script src="lib/jquery.validate.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script>
    $(function() {
        //Ne s'exécute que lorsque la page est complètement chargée
        //Equivalent à document.ready
        $("form#connect").validate();
        $("form#subscribe").validate();
    });
    </script>
</body>

</html>