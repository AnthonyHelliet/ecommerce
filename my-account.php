<?php
// Si aucune session existe alors on redirige l'utilisateur vers l'index
require_once 'back/lib/inc.php';
require 'back/lib/functions.php';
session_start();

if (!isset($_SESSION['user'])) {
    header('location: login.php');
    exit();
} else if ($_SESSION['user']->isBanned == "1") {
    header('location: banned.php');
    exit();
}
$commandes = array();
$commerce = new Commerce();
$annonces = $commerce->getAnnonce();
$commandes = $commerce->getCommande();
$sell = $commerce->getSellInformations();

if (isset($_POST['submit_informations'])) {
    $user = new User();
    $user->username = assainirString($_POST['username']);
    $user->email = assainirString($_POST['mail']);
    $user->nom = assainirString($_POST['nom']);
    $user->prenom = assainirString($_POST['prenom']);
    $user->adresse = assainirString($_POST['adresse']);
    $user->complement_adresse = assainirString($_POST['complement_adresse']);
    $user->code_postal = assainirEntier($_POST['code_postal']);
    $user->commune = assainirString($_POST['commune']);
    $user->id_user = assainirEntier($_POST['id_user']);
    $user->save();
}
if (isset($_POST['submit_password'])) {

    $user = new User();
    $user->password = assainirString($_POST['current_password']);
    $user->id_user = assainirEntier($_POST['id_user']);
    $new_password = assainirString($_POST['password']);
    $user->update_password($user->password, $new_password);
}

if (isset($_POST['delete_product'])) {
    $product = new Product();
    $id_produit = assainirEntier($_POST['id_produit']);
    $product->delete($id_produit);
    header('Location: my-account.php');
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="eCommerce HTML Template Free Download" name="keywords">
    <meta content="eCommerce HTML Template Free Download" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php'?>

    <!-- My Account Start -->
    <div class="my-account">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="dashboard-nav" data-toggle="pill" href="#dashboard-tab"
                            role="tab"><i class="fa fa-tachometer-alt"></i>Dashboard</a>
                        <a class="nav-link" id="orders-nav" data-toggle="pill" href="#orders-tab" role="tab"><i
                                class="fa fa-shopping-bag"></i>Mes commandes</a>
                        <a class="nav-link" id="address-nav" data-toggle="pill" href="#address-tab" role="tab"><i
                                class="fa fa-map-marker-alt"></i>Mes addresses</a>
                        <a class="nav-link" id="account-nav" data-toggle="pill" href="#account-tab" role="tab"><i
                                class="fa fa-user"></i>Informations personnelles</a>
                        <a class="nav-link" href="logout.php"><i class="fa fa-sign-out-alt"></i>Se déconnecter</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="dashboard-tab" role="tabpanel"
                            aria-labelledby="dashboard-nav">
                            <h4>Bonjour <?=$_SESSION['user']->username?></h4>
                            <?= $_SESSION['user']->isBanned ?>
                            <p>Au cours du dernier mois vous avez vendus
                                <?php echo ($sell[0]['id_produit'] > 0 ? $sell[0]['id_produit'] : 0) ?> produits pour
                                une
                                valeur de <?php echo ($sell[0]['prix_total'] > 0 ? $sell[0]['prix_total'] : 0) ?> €</p>
                            <p>Vous avez actuellement <?=count($annonces)?> annonces en cours, </p>
                            <table class="col-md-12">
                                <tbody>
                                    <?php for ($i = 0; $i < count($annonces); $i++) {?>
                                    <tr>
                                        <td style="width:80%"><?=$annonces[$i]->nom_produit?></td>
                                        <td style="width:40%">
                                            <a href="product-detail.php?id_produit=<?=$annonces[$i]->id_produit?>"><i
                                                    class="fa fa-eye"></i></a>
                                            <a href="product-add.php?id_produit=<?=$annonces[$i]->id_produit?>"><i
                                                    class="far fa-edit"></i></a>
                                            <form action="" method="post" id="account_form">
                                                <input type="hidden" name="id_produit"
                                                    value="<?=$annonces[$i]->id_produit?>">
                                                <button type="submit" name="delete_product" class="account_form_delete"><i
                                                        class="far fa-trash-alt"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="orders-tab" role="tabpanel" aria-labelledby="orders-nav">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>No</th>
                                            <th>Date</th>
                                            <th>Total</th>
                                            <th>Statut</th>
                                            <th>Outils</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
                                        if (empty($commandes)) {
                                        ?>
                                            <tr>
                                                <td colspan="5">
                                                    Vous n'avez aucune commande.
                                                </td>
                                            </tr>

                                        <?php
                                        } elseif (!empty($commandes)) {
                                            for ($i = 0; $i < count($commandes); $i++) {?>
                                        <tr>
                                            <td><?=$commandes[$i]->id_commande?></td>
                                            <td><?=$commandes[$i]->date_commande?></td>
                                            <td>€<?=$commandes[$i]->prix_total?></td>
                                            <td>Approved</td>
                                            <td><button class="btn">View</button></td>
                                        </tr>
                                        <?php }}?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="tab-pane fade" id="address-tab" role="tabpanel" aria-labelledby="address-nav">
                            <h4>Addresses</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5>Adresse de facturation</h5>
                                    <p>123 Payment Street, Los Angeles, CA</p>
                                    <p>Mobile: 012-345-6789</p>
                                    <button class="btn">Modifier l'adresse</button>
                                </div>
                                <div class="col-md-6">
                                    <h5>Adresse de livraison</h5>
                                    <p>123 Shipping Street, Los Angeles, CA</p>
                                    <p>Mobile: 012-345-6789</p>
                                    <button class="btn">Modifier l'adresse</button>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="account-tab" role="tabpanel" aria-labelledby="account-nav">
                            <h4>Information personnelles</h4>
                            <form action="" id="personnal_informations" method="POST">
                                <div class="row form-group">
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="username"
                                            placeholder="Nom d'utilisateur" value="<?=$_SESSION['user']->username?>">
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="nom" placeholder="Nom"
                                            value="<?=$_SESSION['user']->nom?>">
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" type="text" name="prenom" placeholder="Prenom"
                                            value="<?=$_SESSION['user']->prenom?>">
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-control" type="text" name="mail" pattern=".+@globex.com"
                                            placeholder="Email" value="<?=$_SESSION['user']->email?>">
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-control" type="text" name="adresse"
                                            value="<?=$_SESSION['user']->adresse?>" placeholder="Adresse">
                                    </div>
                                    <div class="col-md-12">
                                        <input class="form-control" type="text" name="complement_adresse"
                                            placeholder="Complément d'adresse"
                                            value="<?=$_SESSION['user']->complement_adresse?>">
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" type="number" minlength="5" maxlength="5"
                                            name="code_postal" value="<?=$_SESSION['user']->code_postal?>"
                                            placeholder="Code Postal">
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control" type="text" name="commune" placeholder="Commune"
                                            value="<?=$_SESSION['user']->commune?>">
                                    </div>
                                    <div class="col-md-8">
                                        <input class="form-control" type="hidden" name="id_user"
                                            value="<?=$_SESSION['user']->id_user?>">
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" name="submit_informations" value="Mettre a jour"
                                            class="btn">
                                        <br><br>
                                    </div>
                                </div>
                            </form>
                            <h4>Modifier son mot de passe</h4>
                            <form action="" method="POST">
                                <div class="row form-group">
                                    <div class="col-md-12">
                                        <input class="form-control" type="password" name="current_password"
                                            placeholder="Mot de passe actuel">
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="password" name="password"
                                            placeholder="Nouveau de mot de passe">
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="password" name="password_repeat"
                                            placeholder="Confirmez le nouveau mot de passe">
                                    </div>
                                    <div class="col-md-6">
                                        <input class="form-control" type="hidden" name="id_user"
                                            value="<?=$_SESSION['user']->id_user?>">
                                    </div>
                                    <div class="col-md-12">
                                        <input type="submit" name="submit_password" class="btn" value="Mettre à jour">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- My Account End -->

    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script src="back/lib/jquery-3.4.1.min.js"></script>
    <script src="back/lib/jquery.validate.min.js"></script>
    <script>
    $(function() {
        //Ne s'exécute que lorsque la page est complètement chargée
        //Equivalent à document.ready
        $("#personnal_informations").validate();
    });
    </script>
</body>

</html>