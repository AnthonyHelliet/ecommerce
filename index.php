<?php
require_once 'back/lib/inc.php';
require_once 'back/lib/functions.php';
session_start();

$commerce = new Commerce();
$products = $commerce->getProduct();

if (!empty($_POST)) {
    $commerceCard = new Commerce();
    $produit_id = assainirEntier($_POST['produit_id']);
    $commerceCard->addCart($produit_id);
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap" rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php' ?>

    <!-- Recent Product Start -->
    <div class="recent-product product">
        <div class="container-fluid">
            <div class="section-header">
                <h1>Annonces récentes</h1>
            </div>

            <div class="row align-items-center product-slider product-slider-4">
                <?php for ($i = 0; $i < count($products); $i++) { ?>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="product-title">
                                <a href="#"><?= $products[$i]->nom_produit ?></a>
                            </div>
                            <div class="product-image">
                                <a href="product-detail.html">
                                    <img src="<?= $products[$i]->images ?>" alt="Product Image">
                                </a>
                                <div class="product-action">
                                    <form action="" method="POST">
                                        <input type="hidden" name="produit_id" value="<?= $products[$i]->id_produit ?>">
                                        <button class="btn" type="submit"><i class="fa fa-cart-plus"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="product-price">
                                <h3><span>€</span><?= $products[$i]->prix ?></h3>
                                <a class="btn" href="product-detail.php?id_produit=<?= $products[$i]->id_produit ?>"><i class="fa fa-shopping-cart"></i>Voir l'annonce</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Recent Product End -->

    <!-- Featured Product Start -->
    <div class="featured-product product">
        <div class="container-fluid">
            <div class="section-header">
                <h1>Mis en avant</h1>
            </div>
            <div class="row align-items-center product-slider product-slider-4">
                <?php for ($i = 0; $i < count($products); $i++) { ?>
                    <div class="col-md-3">
                        <div class="product-item">
                            <div class="product-title">
                                <a href="#"><?= $products[$i]->nom_produit ?></a>
                            </div>
                            <div class="product-image">
                                <a href="product-detail.html">
                                    <img src="<?= $products[$i]->images ?>" alt="Product Image">
                                </a>
                                <div class="product-action">
                                    <form action="" method="POST">
                                        <input type="hidden" name="produit_id" value="<?= $products[$i]->id_produit ?>">
                                        <button class="btn" type="submit"><i class="fa fa-cart-plus"></i></button>
                                    </form>
                                </div>
                            </div>
                            <div class="product-price">
                                <h3><span>€</span><?= $products[$i]->prix ?></h3>
                                <a class="btn" href="product-detail.php?id_produit=<?= $products[$i]->id_produit ?>"><i class="fa fa-shopping-cart"></i>Voir l'annonce</a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Featured Product End -->

    <?php require_once 'footer.php' ?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>