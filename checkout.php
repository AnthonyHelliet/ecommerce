<?php
// Si aucune session existe alors on redirige l'utilisateur vers l'index
require_once 'back/lib/inc.php';
session_start();

if (!isset($_SESSION['user'])) {
    header('location: login.php');
    exit();
} else if ($_SESSION['user']->isBanned == 1) {
    header('location: banned.php');
    exit();
}

$sous_tot = 0;
$obj = unserialize($_COOKIE['card']);
for ($i = 0; $i < count($obj); $i++) {
    $sous_tot += $obj[$i]->prix;
}

if (!empty($_POST)) {
    $commande = new Commande();
    $commande->id_user = $_SESSION['user']->id_user;
    $commande->prix_total = number_format($sous_tot + $sous_tot * (1 / 100.4), 2);
    $commande->save();
    for ($i = 0; $i < count($obj); $i++) {
        $commande_produit = new Commande_produit();
        $commande_produit->id_produit = $obj[$i]->id_produit;
        $commande_produit->id_commande = $commande->id_commande;
        $commande_produit->prix_total = $obj[$i]->prix;
        $commande_produit->save();
    }
    unset($_COOKIE['card']);
    setcookie('card', null, -1, '/');
    header("Location: index.php");
}
?>


<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="eCommerce HTML Template Free Download" name="keywords">
    <meta content="eCommerce HTML Template Free Download" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>

    <?php require_once 'navbar.php'?>

    <!-- Checkout Start -->
    <div class="checkout">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="checkout-inner">
                        <div class="billing-address">
                            <h2>Adresse de facturation</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nom</label>
                                    <input class="form-control" type="text" placeholder="Nom"
                                        value="<?=$_SESSION['user']->nom?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Prénom</label>
                                    <input class="form-control" type="text" placeholder="Prénom"
                                        value="<?=$_SESSION['user']->prenom?>">
                                </div>
                                <div class="col-md-12">
                                    <label>Email</label>
                                    <input class="form-control" type="text" placeholder="Email"
                                        value="<?=$_SESSION['user']->email?>">
                                </div>
                                <div class="col-md-12">
                                    <label>Addresse</label>
                                    <input class="form-control" type="text" placeholder="Addresse"
                                        value="<?=$_SESSION['user']->adresse?>">
                                </div>
                                <div class="col-md-12">
                                    <label>Complément d'adresse</label>
                                    <input class="form-control" type="text" placeholder="Complément d'adresse"
                                        value="<?=$_SESSION['user']->complement_adresse?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Ville</label>
                                    <input class="form-control" type="text" placeholder="Ville"
                                        value="<?=$_SESSION['user']->commune?>">
                                </div>
                                <div class="col-md-6">
                                    <label>Code postal</label>
                                    <input class="form-control" type="number" placeholder="Code postal"
                                        value="<?=$_SESSION['user']->code_postal?>">
                                </div>
                                <div class="col-md-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="shipto">
                                        <label class="custom-control-label" for="shipto">Envoyer à une autre
                                            adresse</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="shipping-address">
                            <h2>Adresse de livraison</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Nom</label>
                                    <input class="form-control" type="text" placeholder="Nom">
                                </div>
                                <div class="col-md-6">
                                    <label>Prénom</label>
                                    <input class="form-control" type="text" placeholder="Prénom">
                                </div>
                                <div class="col-md-12">
                                    <label>Email</label>
                                    <input class="form-control" type="text" placeholder="Email">
                                </div>
                                <div class="col-md-12">
                                    <label>Addresse</label>
                                    <input class="form-control" type="text" placeholder="Addresse">
                                </div>
                                <div class="col-md-12">
                                    <label>Complément d'addresse</label>
                                    <input class="form-control" type="text" placeholder="Complément d'addresse">
                                </div>
                                <div class="col-md-6">
                                    <label>Ville</label>
                                    <input class="form-control" type="text" placeholder="Ville">
                                </div>
                                <div class="col-md-6">
                                    <label>Code postal</label>
                                    <input class="form-control" type="number" placeholder="Code postal">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="checkout-inner">
                        <div class="checkout-summary">
                            <h1>Mon panier</h1>
                            <?php for ($i = 0; $i < count($obj); $i++) {?>
                            <p><?=$obj[$i]->nom_produit?><span>€<?=$obj[$i]->prix?></span></p>
                            <?php }?>
                            <p class="sub-total">Sous-total<span>€<?=$sous_tot?></span></p>
                            <p class="ship-cost">Frais de livraison<span><?=$sous_tot * (1 / 100)?></span></p>
                            <h2>Total<span>€<?=number_format($sous_tot + $sous_tot * (1 / 100.4), 2)?></span></h2>

                        </div>

                        <div class="checkout-payment">
                            <div class="checkout-btn">
                                <form action="" method="POST">
                                    <?php 
                                    if (count($obj) > 0) { ?>
                                        <button class="btn" type="submit" name="submit">Commander</button>
                                    <?php 
                                    }
                                    ?>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Checkout End -->

    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>