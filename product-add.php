<?php
require_once 'back/lib/inc.php';
require_once 'back/lib/functions.php';
session_start();

if (!isset($_SESSION['user'])) {
    header('location: login.php');
    exit();
} else if ($_SESSION['user']->isBanned == "1") {
    header('location: banned.php');
    exit();
}

$commerce = new Commerce();
$categories = $commerce->getCategorie();

if (!empty($_POST)) {
    $product = new Product();
    if (isset($_GET['id_produit'])) {
        $id_produit = assainirEntier($_GET['id_produit']);
        $product->id_produit = $id_produit;
    }
    $product->id_categorie = assainirEntier($_POST['id_categorie']);
    $product->nom_produit = assainirString($_POST['nom_produit']);
    $product->description_produit = assainirString($_POST['description_produit']);
    $product->desc_courte_produit = assainirString($_POST['description_courte_produit']);
    $product->prix = assainirFloat($_POST['prix']);
    $product->qte = assainirEntier($_POST['quantite']);
    if (($_FILES['fileToUpload']['name'] != "")) {
        // Where the file is going to be stored
        $counter = 0;
        $target_dir = "back/img/";
        if (!is_dir($target_dir)) {
            mkdir($target_dir);
        }
        $file = $_FILES['fileToUpload']['name'];
        $path = pathinfo($file);
        $filename = $path['filename'];
        $ext = $path['extension'];
        $temp_name = $_FILES['fileToUpload']['tmp_name'];
        $path_filename_ext = $target_dir . $filename . "." . $ext;
        if ($ext == "png" || $ext == "jpg") {
            if (file_exists($path_filename_ext)) {
                $i = 1;
                $new_path = $path_filename_ext;

                while (file_exists($new_path)) {
                    $new_filename = $filename . '-' . $i . '.' . $ext;
                    $new_path = $target_dir . $new_filename;
                    $i++;
                }
                move_uploaded_file($temp_name, $new_path);
                $product->images = $new_path;
            } else {
                move_uploaded_file($temp_name, $path_filename_ext);
                $product->images = $path_filename_ext;
            }
        } else {
        }
    } else {
        $product->images = $informations->images;
    }

    $product->id_user = assainirEntier($_POST['id_user']);
    $product->save();
    header("Location: product-detail.php?id_produit=" . $product->id_produit);

}
if (isset($_GET['id_produit'])) {
    $id_produit = assainirEntier($_GET['id_produit']);
    $informations = Product::construit_product($id_produit);
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="eCommerce HTML Template Free Download" name="keywords">
    <meta content="eCommerce HTML Template Free Download" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php'?>

    <div class="container-fluid">
        <div class="row">
            <!-- Product Add Start -->
            <form method="post" enctype="multipart/form-data" id="form" style="width: 100%;" class="register-form">
                <div class=" row form-group">
                    <input type="hidden" name="id_user" value="<?=$_SESSION['user']->id_user?>">
                    <div class="col-md-3">
                        <select name="id_categorie" class="form-control" id="">
                            <option value="<?=isset($_GET['id_produit']) ? $informations->id_categorie : ""?>">
                                <?=isset($_GET['id_produit']) ? "Choisir ou pas toucher" : "Choisir catégorie"?>
                            </option>
                            <?php
for ($i = 0; $i < count($categories); $i++) {
    ?>
                            <option value="<?=$categories[$i]->id_categorie?>"><?=$categories[$i]->nom_categorie?>
                            </option>
                            <?php
}
?>
                        </select>
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="nom_produit" placeholder="Votre titre d'annonce"
                            value="<?=isset($_GET['id_produit']) ? $informations->nom_produit : ''?>">
                    </div>
                    <div class="col-md-2">
                        <input type="number" step="0.1" class="form-control" name="prix" placeholder="prix"
                            value="<?=isset($_GET['id_produit']) ? $informations->prix : ''?>">
                    </div>

                    <div class="col-md-2">
                        <input type="number" class="form-control" name="quantite" placeholder="quantité"
                            value="<?=isset($_GET['id_produit']) ? $informations->qte : ''?>">
                    </div>

                    <div class="col-md-12">
                        <textarea placeholder="Une description courte de votre annonce" class="form-control"
                            name="description_produit" style="width: 100%; max-width: 100%;"
                            rows="4"><?=isset($_GET['id_produit']) ? $informations->description_produit : ''?></textarea>
                    </div>
                    <div class="col-md-12">
                        <textarea placeholder="Détaillez votre annonce pour avoir les meilleurs chances de vendre..."
                            class="form-control" name="description_courte_produit" style="width: 100%; max-width: 100%;"
                            rows="8"><?=isset($_GET['id_produit']) ? $informations->desc_courte_produit : ''?></textarea>
                    </div>
                    <div class="col-md-12">
                        <label
                            for="fileInput"><?=isset($_GET['id_produit']) ? 'Modifier votre photo ou pas toucher :' : 'Ajouter votre photo :'?></label>
                        <input type="file" class="form-control-file" name="fileToUpload" id="fileInput"
                            value="<?=isset($_GET['id_produit']) ? $informations->images : ''?>">

                    </div>
                    <div class="col-md-12" style="margin-top: 1rem;">
                        <input type="submit" class="btn"
                            value="<?=isset($_GET['id_produit']) ? 'Modifier votre annonce' : 'Ajouter une annonce'?>">
                    </div>
                </div>
            </form>
            <!-- Product Add End -->
        </div>
    </div>



    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
    <script src="lib/jquery.validate.min.js"></script>
    <script>
    $(function() {
        //Ne s'exécute que lorsque la page est complètement chargée
        //Equivalent à document.ready
        $("form").validate();
    });
    </script>
</body>

</html>