<?php
// Si aucune session existe alors on redirige l'utilisateur vers l'index
require_once 'back/lib/inc.php';
require_once 'back/lib/functions.php';
session_start();

if (!isset($_SESSION['user'])) {
    header('location: login.php');
    exit();

} else if ($_SESSION['user']->isBanned == "1") {
    header('location: banned.php');
    exit();
}

if (!empty($_POST)) {
    $id_cookie = assainirEntier($_POST['id_cookie']);

    if (isset($_COOKIE['card'])) {
        $obj = unserialize($_COOKIE['card']);
        unset($obj[$id_cookie]);
        setcookie('card', serialize(array_values($obj)), time() + 31556926, '/');
        header('Location: cart.php');
    }
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>La bonne trouvaille</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="eCommerce HTML Template Free Download" name="keywords">
    <meta content="eCommerce HTML Template Free Download" name="description">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Source+Code+Pro:700,900&display=swap"
        rel="stylesheet">

    <!-- CSS Libraries -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/slick/slick-theme.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <?php require_once 'navbar.php'?>

    <!-- Cart Start -->
    <div class="cart-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-8">
                    <div class="cart-page-inner">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Annonce</th>
                                        <th>Prix</th>
                                        <th>Supprimer du panier</th>
                                    </tr>
                                </thead>
                                <tbody class="align-middle">
                                    <?php if (count($obj) == 0) {?>
                                    <tr>
                                        <td colspan="3">Vous n'avez aucun article dans votre panier</td>
                                    </tr>
                                    <?php
} else {
    for ($i = 0; $i < count($obj); $i++) {
        ?>
                                    <tr>
                                        <td>
                                            <div class="img">
                                                <a href="#"><img src="<?=$obj[$i]->images?>" alt="Image"></a>
                                                <p><?=$obj[$i]->nom_produit?></p>
                                            </div>
                                        </td>
                                        <td>€<?=$obj[$i]->prix?></td>
                                        <td>
                                            <form action="" name="delete_cookie" method="post">
                                                <button type="submit"><i class="fa fa-trash"></i></button>
                                                <input type="hidden" name="id_cookie" value="<?=$i?>">
                                            </form>

                                        </td>
                                    </tr>

                                    <?php
}
    ?>

                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="cart-page-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="cart-summary">
                                    <div class="cart-content">
                                        <div class="cart-content">
                                            <p>Sous-total<span><?=$grand_total?> €</span></p>
                                            <p>Frais de
                                                livraison<span><?=number_format($grand_total * (1 / 100), 2)?></span>
                                            </p>
                                            <h2>Total<span><?=number_format($grand_total + $grand_total * (1 / 100), 2)?>
                                                    €</span></h2>
                                        </div>
                                    </div>
                                    <div class="cart-btn">
                                        <a class="btn" href="checkout.php">Passer commande</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->

    <?php require_once 'footer.php'?>

    <!-- Back to Top -->
    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/slick/slick.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>