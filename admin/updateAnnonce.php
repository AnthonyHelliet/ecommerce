<?php
require_once "Class/class-commerce.php";
require_once "Class/class-product.php";
session_start();

$commerce = new Commerce();
$categories = $commerce->getCategorie();

if (!isset($_SESSION['isAdmin'])) {
    header('Location:login.php');
    exit();
}

if (isset($_SESSION['isAdmin'])) {
    if (isset($_GET['id_produit'])) {
        $id_produit = intval($_GET['id_produit']);
        $informations = Product::construit_product($id_produit);
        // var_dump($informations);
        if (!empty($_POST)) {

            $Objproduit = new Product();
            $Objproduit->id_produit = intval($_POST['id_produit']);
            $Objproduit->id_categorie = intval($_POST['id_categorie']);
            $Objproduit->nom_produit = $_POST['nom_produit'];
            $Objproduit->desc_courte_produit = $_POST['desc_courte_produit'];
            $Objproduit->description_produit = $_POST['desc_produit'];
            //echo $Objproduit->id_produit;
            $Objproduit->prix = floatval($_POST['prix']);
            $Objproduit->qte = intval($_POST['quantite']);
            if (($_FILES['fileToUpload']['name'] != "")) {
                echo 'image  dans file';
                // Where the file is going to be stored
                $counter = 0;
                $target_dir = "../back/img/";
                if (!is_dir($target_dir)) {
                    mkdir($target_dir);
                }
                $file = $_FILES['fileToUpload']['name'];
                $path = pathinfo($file);
                $filename = $path['filename'];
                $ext = $path['extension'];
                $temp_name = $_FILES['fileToUpload']['tmp_name'];
                $path_filename_ext = $target_dir . $filename . "." . $ext;
                if ($ext == "png" || $ext == "jpg") {
                    if (file_exists($path_filename_ext)) {
                        $i = 1;
                        $new_path = $path_filename_ext;

                        while (file_exists($new_path)) {
                            $new_filename = $filename . '-' . $i . '.' . $ext;
                            $new_path = $target_dir . $new_filename;
                            $i++;
                        }
                        move_uploaded_file($temp_name, $new_path);
                        echo "new path $new_path";
                        $Objproduit->images = $new_path;
                    } else {
                        move_uploaded_file($temp_name, $path_filename_ext);
                        echo "path $path_filename_ext";
                        $Objproduit->images = $path_filename_ext;
                    }
                } else {
                }
            } else {
                echo 'pas image file';
                $Objproduit->images = $informations->images;
            }
            $Objproduit->id_user = intval($informations->id_user);
            $Objproduit->save();
            header('Refresh:0');
        }
    }
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>



    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index.php" class="brand-link">
                <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">Admin</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="index.php" class="nav-link">
                                <i class="nav-icon fas fa-home"></i>
                                <p>
                                    Dashboard
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="utilisateurs.php" class="nav-link">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    Gérer les utilisateurs
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="produits.php" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    Gérer les annonces
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="logout.php" class="nav-link">
                                <i class="nav-icon fas fa-sign-out-alt"></i>
                                <p>
                                    Se déconnecter
                                </p>
                            </a>
                        </li>
                </nav>

            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">DASHBOARD</h1>
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <form action="" method="POST" enctype="multipart/form-data">

                        <input type="hidden" style="display:none;" id="id" name="id_produit" value="<?= $_GET['id_produit'] ?>">
                        <select class="custom-select" name="id_categorie">

                            <option value="<?= isset($_GET['id_produit']) ? $informations->id_categorie : "" ?>">
                                <?= isset($_GET['id_produit']) ? "Choisir ou pas toucher" : "Choisir catégorie" ?>
                            </option>
                            <?php
                            for ($i = 0; $i < count($categories); $i++) {
                            ?>
                                <option value="<?= $categories[$i]->id_categorie ?>"><?= $categories[$i]->nom_categorie ?>
                                </option>
                            <?php
                            }
                            ?>

                        </select>
                        <div>
                            <label for="nomProduit">Nom</label>
                            <input class="form-control" type="text" id="nomProduit" name="nom_produit" value="<?= $informations->nom_produit ?>">
                        </div>
                        <div>
                            <label for="desCourte">Description courte</label>
                            <input class="form-control" type="text" id="desCourte" name="desc_courte_produit" value="<?= $informations->desc_courte_produit ?>">
                        </div>
                        <div>
                            <label for="desc">Description produit</label>
                            <input class="form-control" type="text" id="desc" name="desc_produit" value="<?= $informations->description_produit ?>">
                        </div>
                        <div>
                            <label for="prix">Prix</label>
                            <input class="form-control" type="number" id="prix" name="prix" value="<?= $informations->prix ?>">
                        </div>

                        <div>
                            <label for="qte">Quantité</label>
                            <input class="form-control" type="number" id="qte" name="quantite" value="<?= $informations->qte ?>">
                        </div>
                        <hr>

                        <div class="custom-file">
                            <label class="custom-file-label" for="images">Photo</label>
                            <input class="custom-file-input" type="file" id="images" name="fileToUpload" value="<?= $informations->images ?>">
                        </div>

                        <input class="btn btn-primary mt-3" type="submit" value="Modifier">


                    </form>
                </div> <!-- /.card -->
            </section>
            <!-- /.Left col -->

            <!-- right col -->
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <!-- Summernote -->
    <script src="plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="dist/js/pages/dashboard.js"></script>
</body>

</html>