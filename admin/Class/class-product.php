<?php

class Product
{
    public $id_produit;
    public $id_categorie;
    public $nom_produit;
    public $description_produit;
    public $desc_courte_produit;
    public $prix;
    public $qte;
    public $images;
    public $id_user;

    public function __construct()
    {
        $this->id_produit = 0;
        $this->id_categorie = 0;
        $this->nom_produit = "";
        $this->description_produit = "";
        $this->desc_courte_produit = "";
        $this->prix = 0.0;
        $this->qte = 0;
        $this->images = "";
        $this->id_user = 0;

    }
    public static function construit_product($id)
    {
        $product = new Product();
        $product->load($id);
        return $product;
    }
    public function load($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT * FROM produit WHERE id_produit = :id_produit ");
        $stmt->execute(array(
            ":id_produit" => $id,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
    }

    public function save()
    {
        try {
            $db_connect = db_connect::getInstance();
            if ($this->id_produit) {
                $stmt = $db_connect->prepare("UPDATE produit SET id_categorie=:id_categorie, nom_produit=:nom_produit, description_produit=:description_produit, desc_courte_produit=:desc_courte_produit, prix=:prix, qte=:qte, images=:images
                WHERE id_produit= :id_produit");
                $stmt->execute(array(
                    ":id_categorie" => $this->id_categorie,
                    ":nom_produit" => $this->nom_produit,
                    ":description_produit" => $this->description_produit,
                    ":desc_courte_produit" => $this->desc_courte_produit,
                    ":prix" => $this->prix,
                    ":qte" => $this->qte,
                    ":images" => $this->images,
                    ":id_produit" => $this->id_produit,
                ));
            } else {
                $stmt = $db_connect->prepare("INSERT INTO produit (id_categorie,nom_produit,description_produit,desc_courte_produit, prix, qte, images, id_user)
                VALUES(:id_categorie,:nom_produit,:description_produit,:desc_courte_produit,:prix,:qte,:images,:id_user)");
                $stmt->execute(array(
                    ":id_categorie" => $this->id_categorie,
                    ":nom_produit" => $this->nom_produit,
                    ":description_produit" => $this->description_produit,
                    ":desc_courte_produit" => $this->desc_courte_produit,
                    ":prix" => $this->prix,
                    ":qte" => $this->qte,
                    ":images" => $this->images,
                    ":id_user" => $this->id_user,
                ));
                $this->id_produit = $db_connect->lastInsertId();
            }
        } catch (Exception $e) {
            print $e->getMessage();

        }
    }

    public function delete($id)
    {
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("DELETE FROM produit WHERE id_produit = :id_produit ");
        var_dump($stmt->execute(array(
            ":id_produit" => $id,
        )));
    }

}