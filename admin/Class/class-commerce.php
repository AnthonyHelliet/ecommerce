<?php

require_once "class-singleton.php";
require_once "class-product.php";
require_once "class-categorie.php";

class Commerce
{
    public $products;
    public $categories;
    public $commandes;

    public function __construct()
    {
        $this->products = array();
        $this->categories = array();
    }

    public function getProduct()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT id_produit FROM produit ORDER BY nom_produit ASC");
        $stmt->execute();
        $tableau = $stmt->fetchAll();
        for ($i = 0; $i < count($tableau); $i++) {
            $this->products[] = Product::construit_product($tableau[$i]['id_produit']);
        }
        return $this->products;
    }

    public function getUsers()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT id_user FROM utilisateur ORDER BY nom ASC");
        $stmt->execute();
        $tableau = $stmt->fetchAll();
        for ($i = 0; $i < count($tableau); $i++) {
            $this->users[] = User::construit_user($tableau[$i]['id_user']);
        }
        return $this->users;
    }

    public function getAnnonce()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT id_produit FROM produit WHERE id_user = :id_user ORDER BY nom_produit ASC");
        $stmt->execute(array(
            "id_user" => $_SESSION['user']->id_user,
        ));
        $tableau = $stmt->fetchAll();
        for ($i = 0; $i < count($tableau); $i++) {
            $this->products[] = Product::construit_product($tableau[$i]['id_produit']);
        }
        return $this->products;
    }

    public function getCategorie()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT id_categorie FROM categorie ORDER BY nom_categorie ASC");
        $stmt->execute();
        $tableau = $stmt->fetchAll();
        for ($i = 0; $i < count($tableau); $i++) {
            $this->categories[] = Categorie::construit_categorie($tableau[$i]['id_categorie']);
        }
        return $this->categories;
    }

    public function getCommande()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT id_commande FROM commande WHERE id_user = :id_user");
        $stmt->execute(array(
            ":id_user" => $_SESSION['user']->id_user,
        ));
        $tableau = $stmt->fetchAll();
        for ($i = 0; $i < count($tableau); $i++) {
            $this->commandes[] = Commande::construit_commande($tableau[$i]['id_commande']);
        }
        return $this->commandes;
    }

    public function getSellInformations()
    {
        $tableau = array();
        $db_connect = db_connect::getInstance();
        $stmt = $db_connect->prepare("SELECT COUNT(commande_produit.id_produit) as id_produit, SUM(commande_produit.prix_total) as prix_total FROM commande_produit
        JOIN produit ON commande_produit.id_produit = produit.id_produit
        WHERE produit.id_user = :id_user");
        $stmt->execute(array(
            ":id_user" => $_SESSION['user']->id_user,
        ));
        $tableau = $stmt->fetchAll();
        return $tableau;
    }

    public function addCart($id)
    {
        $old_cookie = unserialize($_COOKIE['card']);
        $old_cookie[] = Product::construit_product($id);
        setcookie('card', serialize($old_cookie), time() + 31556926, '/');
        header('Location: index.php');
    }

}