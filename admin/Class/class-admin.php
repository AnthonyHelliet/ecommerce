<?php
require_once "class-singleton.php";
require_once "class-user.php";
session_start();

class Admin extends User
{
    public $isAdmin;

    public function __construct()
    {
        $this->isAdmin = 0;
    }

    public function connect($password)
    {
        $db_connect = db_connect::getInstance();
        $requete = "SELECT isAdmin,email, password FROM utilisateur WHERE email=:email";
        $stmt = $db_connect->prepare($requete);
        $stmt->execute(array(
            ":email" => $this->email,
        ));
        $stmt->setFetchMode(PDO::FETCH_INTO, $this);
        $stmt->fetch();
        if ($password == $this->password) {

            //Je sauve l'objet dans la session
            if ($this->isAdmin == 1) {
                $_SESSION['isAdmin'] = $this;
                header('Location:index.php');
            } else {
                echo "Mot de passe incorrect";
            }
        }
    }
}
