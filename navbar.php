<?php
require_once 'back/lib/inc.php';
require_once 'back/lib/functions.php';

if (isset($_COOKIE['card'])) {
    $grand_total = 0;
    $obj = unserialize($_COOKIE['card']);
    $count = count($obj);
    for ($i = 0; $i < count($obj); $i++) {
        $grand_total += $obj[$i]->prix;
    }
}
$commerce = new Commerce();
$categories = $commerce->getCategorie();
?>

<!-- Top bar Start -->
<div class="top-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <i class="fa fa-envelope"></i>
                support@labonnetrouvaille.fr
            </div>
            <div class="col-sm-6">
                <i class="fa fa-phone-alt"></i>
                +01-01-01-01-01
            </div>
        </div>
    </div>
</div>
<!-- Top bar End -->

<!-- Nav Bar Start -->
<div class="nav">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
            <a href="#" class="navbar-brand">LA BONNE TROUVAILLE</a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                <div class="navbar-nav mr-auto">
                    <a href="index.php" class="nav-item nav-link">Accueil</a>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Catégories</a>
                        <div class="dropdown-menu">
                            <?php for ($i = 0; $i < count($categories); $i++) {?>
                            <a href="product-list.php" class="dropdown-item"><?=$categories[$i]->nom_categorie?></a>
                            <?php }?>
                        </div>
                    </div>
                    <?php
if (isset($_SESSION['user'])) {
    ?>
                    <a href="product-add.php" class="nav-item nav-link">Ajouter une annonce</a>
                    <?php
}
?>
                    <a href="contact.php" class="nav-item nav-link">Nous contacter</a>
                </div>
                <div class="navbar-nav ml-auto">
                    <!-- Si l'utilisateur n'existe pas -->
                    <?php
if (empty($_SESSION['user'])) {
    ?>
                    <a href="login.php" class="nav-item nav-link">Se connecter / S'enregistrer</a>
                    <!-- Si l'utilisateur est connecté -->
                    <?php
} elseif (isset($_SESSION['user'])) {
    ?>
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link dropdown-toggle"
                            data-toggle="dropdown"><?=$_SESSION['user']->nom . ' ' . $_SESSION['user']->prenom?></a>
                        <div class="dropdown-menu">
                            <a href="my-account.php" class="dropdown-item">Mon compte</a>
                            <a href="cart.php" class="dropdown-item">Mon panier</a>
                            <a href="checkout.php" class="dropdown-item">Ma commande</a>
                            <a href="logout.php" class="dropdown-item">Se déconnecter</a>
                        </div>
                    </div>
                    <?php
}
?>
                </div>
            </div>
        </nav>
    </div>
</div>
<!-- Nav Bar End -->

<!-- Bottom Bar Start -->
<div class="bottom-bar">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-md-3">
                <div class="logo">
                    <a href="index.php">
                        <img src="img/logo.png" alt="Logo">
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="search">
                    <input type="text" placeholder="Rechercher" name="query" id="query">
                    <button><i class="fa fa-search"></i></button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="user">
                    <a href="cart.php" class="btn cart">
                        <i class="fa fa-shopping-cart"></i>
                        <!-- LE NOMBRE DE PRODUITS DANS LE PANIER -->
                        <span> <?php echo (isset($_COOKIE['card']) ? $count : 0) ?></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Bottom Bar End -->